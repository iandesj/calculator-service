var numOnDisplay = '';
var currentOp = '';

$(document).ready(function() {
    $('.num').click(function() {
        if(numOnDisplay === "ERROR") numOnDisplay = '';
        numOnDisplay += $(this).attr("value");
        $('#screen').html(numOnDisplay);
    });

    $('#equals').click(function() {
        operations('');
    });

    $('#add').click(function() {
        operations('+');
    });

    $('#subtract').click(function() {
        operations('-');
    });

    $('#divide').click(function() {
        operations('/');
    });

    $('#multiply').click(function() {
        operations('*');
    });

    $('#clear').click(function() {
        numOnDisplay = '';
        currentOp = '';
        $('#screen').html(numOnDisplay);
    });
});

function performOp(first, second) {
    var basePath = '';
    if(currentOp === '+') basePath = 'add';
    if(currentOp === '-') basePath = 'subtract';
    if(currentOp === '/') basePath = 'divide';
    if(currentOp === '*') basePath = 'multiply';
    $.ajax({
        url: `http://localhost:8080/${basePath}/${first}/${second}`
    }).done(function(data) {
        numOnDisplay = data.result + currentOp;
        $('#screen').html(numOnDisplay);
    }).fail(function() {
        numOnDisplay = "ERROR";
        $('#screen').html(numOnDisplay);
    });
}

function operations(operation) {
    if(numOnDisplay[numOnDisplay.length-1] === currentOp &&
            numOnDisplay[numOnDisplay.length-1] !== operation) {
        var temp = numOnDisplay.split("").reverse();
        temp[0] = operation;
        numOnDisplay = temp.reverse().join("");
        $('#screen').html(numOnDisplay);
    }
    var nums = numOnDisplay.split(currentOp).filter(Boolean);
    if(nums.length > 1 && currentOp) {
        performOp(nums[0], nums[1]);
        currentOp = operation;
    }
    else {
        currentOp = operation;
        if(!numOnDisplay.endsWith(currentOp)) numOnDisplay += currentOp;
        $('#screen').html(numOnDisplay);
    }
}
