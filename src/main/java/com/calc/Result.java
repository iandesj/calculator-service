package com.calc;

/**
 * Created by ian on 9/14/16.
 */
public class Result {

    private Object result;

    public Result(int result) {
        this.result = result;
    }

    public Result(double result) {
        this.result = result;
    }

    public Object getResult() {
        return result;
    }
}
