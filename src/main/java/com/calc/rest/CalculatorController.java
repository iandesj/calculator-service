package com.calc.rest;

import com.calc.Result;
import com.calc.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ian on 9/14/16.
 */
@Controller
public class CalculatorController {

    @Autowired
    private CalculatorService calc;

    @RequestMapping("/add/{first}/{second:.+}")
    public @ResponseBody
    ResponseEntity<?> add(@PathVariable double first, @PathVariable double second) {
        return ResponseEntity.ok(new Result(calc.add(first, second)));
    }

    @RequestMapping("/subtract/{first}/{second:.+}")
    public @ResponseBody
    ResponseEntity<?> subtract(@PathVariable double first, @PathVariable double second) {
        return ResponseEntity.ok(new Result(calc.subtract(first, second)));
    }

    @RequestMapping("/divide/{numerator}/{denominator:.+}")
    public @ResponseBody
    ResponseEntity<?> divide(@PathVariable double numerator, @PathVariable double denominator) {
        try {
            return ResponseEntity.ok(new Result(calc.divide(numerator, denominator)));
        } catch(ArithmeticException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping("/multiply/{first}/{second:.+}")
    public @ResponseBody
    ResponseEntity<?> multiply(@PathVariable double first, @PathVariable double second) {
        return ResponseEntity.ok(new Result(calc.multiply(first, second)));
    }
}
