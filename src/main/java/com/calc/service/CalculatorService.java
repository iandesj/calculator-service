package com.calc.service;

import org.springframework.stereotype.Service;

/**
 * Created by ian on 9/14/16.
 */
@Service
public class CalculatorService {

    public double add(double firstAdded, double secondAddend) {
        return firstAdded + secondAddend;
    }

    public double subtract(double minuend, double subtrahend) {
        return minuend - subtrahend;
    }

    public double divide(double numerator, double denominator) throws ArithmeticException {
        if(denominator == 0.0)
            throw new ArithmeticException("Cannot divide by zero");
        return numerator / denominator;
    }

    public double multiply(double firstFactor, double secondFactor) {
        return firstFactor * secondFactor;
    }
}
