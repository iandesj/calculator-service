package com.calc.service;

import static org.testng.Assert.*;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Created by ian on 9/14/16.
 */
public class CalculatorServiceUTest {

    private CalculatorService calc;

    @BeforeMethod
    public void setUp() {
        calc = new CalculatorService();
    }

    @Test
    public void addTwoIntegerNumbers() {
        double result = calc.add(50, 20);
        assertEquals(70.0, result);
    }

    @Test
    public void addTwoDecimalNumbers() {
        double result = calc.add(50.0, 20.0);
        assertEquals(70.0, result);
    }

    @Test
    public void addDecimalAndInteger() {
        double result = calc.add(50, 20.0);
        assertEquals(70.0, result);
    }

    @Test
    public void subtractTwoIntegerNumbers() {
        double difference = calc.subtract(70, 50);
        assertEquals(20.0, difference);
    }

    @Test
    public void subtractTwoDecimalNumbers() {
        double difference = calc.subtract(70.5, 50.0);
        assertEquals(20.5, difference);
    }

    @Test
    public void subtractDecimalFromInteger() {
        double difference = calc.subtract(70, 50.9);
        assertEquals(19.1, difference);
    }

    @Test
    public void subtractIntegerFromDecimal() {
        double difference = calc.subtract(70.0, 50);
        assertEquals(20.0, difference);
    }

    @Test
    public void divideTwoIntegers() {
        double quotient = calc.divide(5, 2);
        assertEquals(2.5, quotient);
    }

    @Test
    public void divideTwoDecimals() {
        double quotient = calc.divide(50.0, 25.0);
        assertEquals(2.0, quotient);
    }

    @Test
    public void divideDecimalAndInteger() {
        double quotient = calc.divide(3, 2.0);
        assertEquals(1.5, quotient);
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void cannotDivideByZero() {
        calc.divide(3, 0);
    }

    @Test
    public void multiplyTwoIntegers() {
        double product = calc.multiply(5, 6);
        assertEquals(30.0, product);
    }

    @Test
    public void multiplyTwoDecimals() {
        double product = calc.multiply(1.5, 5.0);
        assertEquals(7.5, product);
    }

    @Test
    public void multiplyDecimalAndInteger() {
        double product = calc.multiply(1.5, 5);
        assertEquals(7.5, product);
    }

    @Test
    public void multiplyByZero() {
        double product1 = calc.multiply(5.0, 0.0);
        assertEquals(0.0, product1);
        double product2 = calc.multiply(5, 0);
        assertEquals(0.0, product2);
    }
}
