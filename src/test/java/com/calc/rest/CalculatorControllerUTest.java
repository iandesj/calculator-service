package com.calc.rest;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.calc.service.CalculatorService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 * Created by ian on 9/14/16.
 */
public class CalculatorControllerUTest {

    private MockMvc restMvc;

    @InjectMocks
    private CalculatorController controller;

    @Mock
    private CalculatorService service;

    @BeforeMethod
    public void setup() {
        MockitoAnnotations.initMocks(this);
        restMvc = standaloneSetup(controller).setMessageConverters(new MappingJackson2HttpMessageConverter()).build();
    }

    @Test
    public void testAdd() throws Exception {
        when(service.add(5, 5)).thenReturn(10.0);
        restMvc.perform(get("/add/5/5")).andExpect(status().isOk()).andExpect(jsonPath("$.result").value(10.0));
        verify(service,times(1)).add(5, 5);
    }

    @Test
    public void testSubtract() throws Exception {
        when(service.subtract(50, 5)).thenReturn(45.0);
        restMvc.perform(get("/subtract/50/5")).andExpect(status().isOk()).andExpect(jsonPath("$.result").value(45.0));
        verify(service,times(1)).subtract(50, 5);
    }

    @Test
    public void testDivide() throws Exception {
        when(service.divide(25, 5)).thenReturn(5.0);
        restMvc.perform(get("/divide/25/5")).andExpect(status().isOk()).andExpect(jsonPath("$.result").value(5.0));
        verify(service,times(1)).divide(25, 5);
    }

    @Test
    public void testDivideByZero() throws Exception {
        doThrow(new ArithmeticException("ae")).when(service).divide(25, 0);
        restMvc.perform(get("/divide/25/0")).andExpect(status().isBadRequest());
        verify(service,times(1)).divide(25, 0);
    }

    @Test
    public void testMultiply() throws Exception {
        when(service.multiply(5, 5)).thenReturn(25.0);
        restMvc.perform(get("/multiply/5/5")).andExpect(status().isOk()).andExpect(jsonPath("$.result").value(25.0));
        verify(service,times(1)).multiply(5, 5);
    }
}
