Calculator Service
=============================

Programming exercise for KLA.

I created a microservice with:
- add
- subtract
- divide
- multiply

My approach was to TDD out the domain (CalculatorService), create the controller endpoints, and quickly throw together a static site (src/main/resources/static) with the calculator interfacing with the spring boot service. I then added necessary unit tests for the controller. I did not get to writing any tests for the UI (would probably have done page-object) due to time constraints.

simply clone this repo, jump into the directory and run `gradle clean build bootRun` to ensure it's built, tests pass, and the app can be started. Visit at http://localhost:8080.
